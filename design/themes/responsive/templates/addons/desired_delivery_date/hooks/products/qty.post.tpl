<div class="ty-control-group">
    <label class="ty-control-group__title" for="desired_delivery_date">{__("desired_delivery_date")}</label>
        {include file="common/calendar.tpl" date_id="desired_delivery_date" date_name="product_data[{$obj_id}][extra][desired_delivery_date]" date_val=$product_data.$obj_id.extra.desired_delivery_date|default:''}
</div>