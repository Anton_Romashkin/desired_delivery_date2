{if $cart.products.$obj_id.extra.desired_delivery_date}
	<div class="ty-orders-detail__desired_delivery_date">{__("desired_delivery_date")}:&nbsp;{$cart.products.$obj_id.extra.desired_delivery_date|date_format:$settings.Appearance.date_format}
	</div>
	<input type="hidden" name="cart_products[{$obj_id}][extra][desired_delivery_date]" value="{$cart.products.$obj_id.extra.desired_delivery_date}" />
{/if}