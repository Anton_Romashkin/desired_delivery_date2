{if $product.extra.desired_delivery_date}
	 <div class="ty-orders-detail__desired_delivery_date">{__("desired_delivery_date")}:&nbsp;{$product.extra.desired_delivery_date|date_format:$settings.Appearance.date_format}</div>
{/if}