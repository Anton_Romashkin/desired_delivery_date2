<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'generate_cart_id',
    'add_to_cart',
    'get_order_info'
);