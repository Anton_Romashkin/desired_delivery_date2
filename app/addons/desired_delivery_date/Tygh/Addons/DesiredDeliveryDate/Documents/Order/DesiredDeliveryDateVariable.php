<?php

namespace Tygh\Addons\DesiredDeliveryDate\Documents\Order;

use Tygh\Registry;
use Tygh\Template\Document\Order\Context;
use Tygh\Template\IVariable;

class DesiredDeliveryDateVariable implements IVariable
{
    public $delivery_date;

    public function __construct(Context $context)
    {
        $order = $context->getOrder();
      	foreach ($order->data['product_groups'] as $key => $product) {
      		foreach ($product['products'] as $key => $item) {
      			if(isset($item['desired_delivery_date'])){
      				$this->delivery_date = $item['desired_delivery_date'];
      			}
      		}
      	}
    }
}