<?php

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;
use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_desired_delivery_date_generate_cart_id(&$_cid, $extra, $only_selectable){
    if (!empty($extra['desired_delivery_date'])){
        $extra['desired_delivery_date'] = fn_parse_date($extra['desired_delivery_date']);
        $_cid[] = $extra['desired_delivery_date'];
    }
}

function fn_desired_delivery_date_add_to_cart(&$cart, $product_id, $_id){
    if (!empty($cart['products'][$_id]['extra']['desired_delivery_date'])){
        $cart['products'][$_id]['extra']['desired_delivery_date'] = fn_parse_date($cart['products'][$_id]['extra']['desired_delivery_date']);
    }   
}

function fn_desired_delivery_date_get_order_info(&$order, $additional_data){
    foreach ($order['products'] as &$product) {
        $product['desired_delivery_date'] = isset($product['extra']['desired_delivery_date']) ?  $product['extra']['desired_delivery_date'] : 0;

        if (!empty($product['desired_delivery_date'])){
            $product['desired_delivery_date_formatted'] = Tygh::$app['formatter'] -> asDatetime($product['desired_delivery_date']);
        }
    }
}