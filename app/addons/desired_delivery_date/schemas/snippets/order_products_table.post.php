<?php
	$schema['sd_p'] = [
		'class' => '\Tygh\Template\Document\Variables\GenericVariable',
		'data' => function (\Tygh\Template\Snippet\Table\ItemContext $context){
			$product = $context->getItem();

			return $product;
		},
		'arguments' => ['#context', '#config', '@formatter'],
		'attributes' => [
			'desired_delivery_date_formatted',
		],
	];

	return $schema;